package examples

import llvm.codegen.instructions.terminator.ret
import llvm.codegen.irBuilder
import llvm.codegen.syntax.stringifiers.ir

fun main() {
    irBuilder {
        main {
            entry {
                +ret(123.ir)
            }
        }
    }.let { irb ->
        println(irb.compileToHumanReadableIR())
    }
}
