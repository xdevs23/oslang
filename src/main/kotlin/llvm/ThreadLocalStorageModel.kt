package llvm

import llvm.codegen.syntax.IRPiece

/**
 * https://llvm.org/docs/LangRef.html#thread-local-storage-models
 */
abstract class ThreadLocalStorageModel : IRPiece()

/**
 * https://llvm.org/docs/LangRef.html#thread-local-storage-models
 */
object ThreadLocalStorageModels {

    object LocalDynamic : ThreadLocalStorageModel() {
        override fun text() = "default"
    }

    object InitialExec : ThreadLocalStorageModel() {
        override fun text() = "initialexec"
    }

    object LocalExec : ThreadLocalStorageModel() {
        override fun text() = "localexec"
    }

}
