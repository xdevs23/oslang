package llvm

import llvm.codegen.syntax.IRPiece

/**
 * https://llvm.org/docs/LangRef.html#dll-storage-classes
 */
abstract class DLLStorageClass : IRPiece()

/**
 * https://llvm.org/docs/LangRef.html#dll-storage-classes
 */
object DLLStorageClasses {

    object Import : DLLStorageClass() {
        override fun text() = "dllimport"
    }

    object Export : DLLStorageClass() {
        override fun text() = "dllexport"
    }

}
