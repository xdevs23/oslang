package llvm.codegen

import llvm.codegen.instructions.terminator.ret
import llvm.codegen.syntax.IRValueIdentifier
import llvm.codegen.syntax.globalIdentifier
import llvm.codegen.syntax.stringifiers.ir
import llvm.codegen.syntax.types.IRType
import llvm.codegen.syntax.types.IRTypes

class IRBuilder(val context: IRContext) {

    fun function(
        returnType: IRType, identifier: IRValueIdentifier, parameters: IRFunctionParameters = listOf(),
        body: IRFunction.() -> Unit,
    ) {
        context.module.run {
            +IRFunction(context, returnType, identifier, parameters, body).define
        }
    }

    fun main(body: IRFunction.() -> Unit) {
        function(IRTypes.I32, "main".globalIdentifier) {
            body()
        }
    }

    fun compileToHumanReadableIR() = context.module.toString()

    override fun toString() = compileToHumanReadableIR()
}

inline fun irBuilder(block: IRBuilder.() -> Unit) = IRBuilder(IRContext()).apply(block)
