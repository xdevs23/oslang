package llvm.codegen

import llvm.codegen.syntax.types.IRTypes

class IRBasicBlock(
    context: IRContext,
    name: IRTypes.NamedLabel,
    code: IRBasicBlock.() -> Unit,
) : IRBlockWithBody<IRBasicBlock>(context, code) {
    override val indentation = 0

    init {
        +name.value()
        code()
    }
}
