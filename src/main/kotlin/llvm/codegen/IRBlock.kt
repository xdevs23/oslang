package llvm.codegen

import llvm.codegen.syntax.IRBlock
import llvm.codegen.syntax.IRPieceLike
import java.util.*

interface IRBlockWithBodyLike {
    val indentation: Int
    fun addPiece(piece: IRPieceLike)
    operator fun IRPieceLike.unaryPlus()
}

const val defaultIndentation = 0

abstract class IRBlockWithBody<S : IRBlockWithBodyLike>(
    val context: IRContext,
    val code: S.() -> Unit,
) : IRBlock(), IRBlockWithBodyLike {
    private val pieces = LinkedList<IRPieceLike>()

    override fun pieces() = pieces

    override fun addPiece(piece: IRPieceLike) {
        pieces += piece
    }

    override operator fun IRPieceLike.unaryPlus() {
        addPiece(this)
    }

}

open class IRBlockWrapper(
    context: IRContext,
    block: IRBlockWrapper.() -> Unit,
) : IRBlockWithBody<IRBlockWrapper>(context, block)

fun IRBlockWithBody<*>.wrap(block: IRBlockWrapper.() -> Unit) = IRBlockWrapper(context, block)
