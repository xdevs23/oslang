package llvm.codegen

import llvm.codegen.syntax.IRDelegatedPiece
import llvm.codegen.syntax.IRPiece
import llvm.codegen.syntax.IRPieceLike
import llvm.codegen.syntax.stringifiers.spaced
import llvm.codegen.syntax.types.IRType
import llvm.codegen.syntax.types.IRTypes

interface IRValueLike : IRPieceLike {
    val type: IRType
    fun value(): IRPieceLike
}
abstract class IRValue : IRDelegatedPiece(), IRValueLike {
    override fun piece() = listOf(type, value()).spaced
}
