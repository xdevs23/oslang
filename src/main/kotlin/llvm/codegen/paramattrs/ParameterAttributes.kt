package llvm.codegen.paramattrs

import llvm.codegen.syntax.IRPiece
import llvm.codegen.syntax.IRPieceLike
import llvm.codegen.syntax.stringifiers.combined
import llvm.codegen.syntax.stringifiers.enclosed
import llvm.codegen.syntax.stringifiers.ir
import llvm.codegen.syntax.stringifiers.rawIR
import llvm.codegen.syntax.types.IRType

/**
 * https://llvm.org/docs/LangRef.html#parameter-attributes
 */
abstract class ParameterAttribute : IRPiece()
abstract class SimpleParameterAttribute(private val gen: () -> IRPieceLike) : ParameterAttribute() {
    override fun text() = gen().text()
}
abstract class NamedParameterAttribute(private val name: String) : ParameterAttribute() {
    override fun text() = name
}

object ParameterAttributes {
    object ZeroExt : NamedParameterAttribute("zeroext")
    object SignExt : NamedParameterAttribute("signext")
    object InReg : NamedParameterAttribute("inreg")
    class ByVal(ty: IRType) : SimpleParameterAttribute({ listOf("byval".rawIR, ty.enclosed).combined })
    class ByRef(ty: IRType) : SimpleParameterAttribute({ listOf("byref".rawIR, ty.enclosed).combined })
    class PreAllocated(ty: IRType) : SimpleParameterAttribute({ listOf("prellocated".rawIR, ty.enclosed).combined })
    class InAlloca(ty: IRType) : SimpleParameterAttribute({ listOf("inalloca".rawIR, ty.enclosed).combined })
    class SRet(ty: IRType) : SimpleParameterAttribute({ listOf("sret".rawIR, ty.enclosed).combined })
    class ElementType(ty: IRType) : SimpleParameterAttribute({ listOf("elementtype".rawIR, ty.enclosed).combined })
    class Align(ty: IRType) : SimpleParameterAttribute({ listOf("align".rawIR, ty.enclosed).combined })
    object NoAlias : NamedParameterAttribute("noalias")
    object NoCapture : NamedParameterAttribute("nocapture")
    object NoFree : NamedParameterAttribute("nofree")
    object Nest : NamedParameterAttribute("nest")
    object Returned : NamedParameterAttribute("returned")
    class Deferenceable(ty: IRType) : SimpleParameterAttribute({ listOf("deferenceable".rawIR, ty.enclosed).combined })
    class DeferenceableOrNull(ty: IRType) :
        SimpleParameterAttribute({ listOf("dereferenceable_or_null".rawIR, ty.enclosed).combined })
    object SwiftSelf : NamedParameterAttribute("swiftself")
    object SwiftAsync : NamedParameterAttribute("swiftasync")
    object SwiftError : NamedParameterAttribute("swifterror")
    object ImmArg : NamedParameterAttribute("immarg")
    object NoUndef : NamedParameterAttribute("noundef")
    class AlignStack(ty: IRType) : SimpleParameterAttribute({ listOf("alignstack".rawIR, ty.enclosed).combined })
    object AllocAlign : NamedParameterAttribute("allocalign")
    object AllocPtr : NamedParameterAttribute("allocptr")
}
