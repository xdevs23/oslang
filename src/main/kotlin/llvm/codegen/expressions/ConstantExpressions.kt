package llvm.codegen.expressions

import llvm.codegen.constants.IRConstantLike
import llvm.codegen.syntax.stringifiers.ir
import llvm.codegen.syntax.stringifiers.rawIR

// https://llvm.org/docs/LangRef.html#constant-expressions

abstract class IRConstantExpression : IRExpression(), IRConstantLike
open class IRNamedConstantExpression(private val name: String) : IRConstantExpression() {
    override fun piece() = name.rawIR
}

object IRConstantExpressions {
    object Trunc : IRNamedConstantExpression("trunc")
    object ZExt : IRNamedConstantExpression("zext")
    object SExt : IRNamedConstantExpression("sext")
    object FPTrunc : IRNamedConstantExpression("fptrunc")
    object FPExt : IRNamedConstantExpression("fpext")
    object FPToUI : IRNamedConstantExpression("fptoui")
    object FPToSI : IRNamedConstantExpression("fptosi")
    object UIToFP : IRNamedConstantExpression("uitofp")
    object SIToFP : IRNamedConstantExpression("sitofp")
    object PtrToInt : IRNamedConstantExpression("ptrtoint")
    object IntToPtr : IRNamedConstantExpression("inttoptr")
    object BitCast : IRNamedConstantExpression("bitcast")
    object AddrSpaceCast : IRNamedConstantExpression("addrspacecast")
    object GetElementPtr : IRNamedConstantExpression("getelementptr")
    object Select : IRNamedConstantExpression("select")
    object ICmp : IRNamedConstantExpression("icmp")
    object FCmp : IRNamedConstantExpression("fcmp")
    object ExtractElement : IRNamedConstantExpression("extractelement")
    object InsertElement : IRNamedConstantExpression("insertelement")
    object ShuffleVector : IRNamedConstantExpression("shufflevector")
    object ExtractValue : IRNamedConstantExpression("extractvalue")
    object InsertValue : IRNamedConstantExpression("insertvalue")
    class Operation(val opcode: IRConstantLike, val lhs: IRConstantLike, val rhs: IRConstantLike)
}

