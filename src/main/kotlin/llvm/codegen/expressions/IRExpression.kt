package llvm.codegen.expressions

import llvm.codegen.syntax.IRDelegatedPiece
import llvm.codegen.syntax.IRPieceLike

interface IRExpressionLike : IRPieceLike
abstract class IRExpression : IRDelegatedPiece(), IRExpressionLike
