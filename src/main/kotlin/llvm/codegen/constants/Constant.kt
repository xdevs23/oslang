package llvm.codegen.constants

import llvm.codegen.IRValue
import llvm.codegen.syntax.IRPiece
import llvm.codegen.syntax.IRPieceLike
import llvm.codegen.syntax.stringifiers.rawIR
import llvm.codegen.syntax.types.IRType

interface IRConstantLike : IRPieceLike

/**
 * https://llvm.org/docs/LangRef.html#constants
 */
abstract class IRConstant : IRValue(), IRConstantLike

abstract class IRSimpleConstant(val gen: () -> IRPiece, override val type: IRType) : IRConstant() {
    override fun value() = gen()
}

abstract class IRNamedConstant(private val name: String, type: IRType) : IRSimpleConstant({ name.rawIR }, type)
