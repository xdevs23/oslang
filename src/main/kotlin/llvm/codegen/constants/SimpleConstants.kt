package llvm.codegen.constants

import llvm.codegen.syntax.stringifiers.ir
import llvm.codegen.syntax.stringifiers.rawIr
import llvm.codegen.syntax.types.IRType
import llvm.codegen.syntax.types.IRTypes

// https://llvm.org/docs/LangRef.html#simple-constants

sealed class BooleanConstant(value: Boolean) : IRNamedConstant(value.toString(), IRTypes.I1), IRTypes.I1Type
object TrueBooleanConstant : BooleanConstant(true)
object FalseBooleanConstant : BooleanConstant(false)

inline val `true` get() = TrueBooleanConstant
inline val `false` get() = FalseBooleanConstant

open class IntegerConstant(value: Int) : IRSimpleConstant({ value.ir }, IRTypes.I64), IRTypes.I64Type
open class FloatingPointConstant(value: Float) :
    IRSimpleConstant({ value.rawIr }, IRTypes.Float), IRTypes.FloatingPointType

class NullPointerConstant : IRNamedConstant("null", IRTypes.Pointer), IRTypes.PointerType

class NoneTokenConstant : IRNamedConstant("none", IRTypes.Token), IRTypes.TokenType
