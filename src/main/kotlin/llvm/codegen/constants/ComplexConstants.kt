package llvm.codegen.constants

import llvm.codegen.syntax.IRPiece
import llvm.codegen.syntax.stringifiers.rawIR
import llvm.codegen.syntax.types.IRTypes

// https://llvm.org/docs/LangRef.html#complex-constants

object ZeroInitializerConstantType : IRTypes.NamedComplexConstant("zeroinitializer")
class ZeroInitializerConstant : IRSimpleConstant({ ZeroInitializerConstantType }, ZeroInitializerConstantType)

// TODO
class MetadataNode()
