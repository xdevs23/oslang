package llvm.codegen

import llvm.codegen.syntax.IRBlock
import llvm.codegen.syntax.IRPieceLike
import llvm.codegen.syntax.stringifiers.rawIR
import llvm.codegen.syntax.stringifiers.spaced

open class IRDefine(private val piece: IRPieceLike) : IRBlock() {
    override val indentation = 0
    override fun pieces() = listOf(listOf("define".rawIR, piece).spaced)
}

val IRPieceLike.define get() = IRDefine(this)
