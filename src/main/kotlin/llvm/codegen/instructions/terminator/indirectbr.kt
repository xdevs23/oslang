package llvm.codegen.instructions.terminator

import llvm.codegen.instructions.IRInstruction
import llvm.codegen.syntax.IRBasicBlockAddress
import llvm.codegen.syntax.stringifiers.*
import llvm.codegen.syntax.types.IRTypes

/**
 * https://llvm.org/docs/LangRef.html#indirectbr-instruction
 */
class IRInstructionIndirectBr(
    private val address: IRBasicBlockAddress,
    private val destinations: List<IRTypes.LabelType>,
) : IRInstruction() {
    override fun piece() = listOf(
        listOf("indirectbr".rawIR, IRTypes.Pointer, address).spaced,
        destinations.map { listOf(IRTypes.Label, it).spaced }.commaSeparated.padded.bracketed,
    ).commaSeparated
}

fun indirectbr(
    address: IRBasicBlockAddress,
    destinations: List<IRTypes.LabelType>,
) = IRInstructionIndirectBr(address, destinations)
