package llvm.codegen.instructions.terminator

import llvm.codegen.IRValueLike
import llvm.codegen.instructions.IRSimpleInstruction
import llvm.codegen.syntax.IRPieceLike
import llvm.codegen.syntax.stringifiers.ir
import llvm.codegen.syntax.stringifiers.rawIR
import llvm.codegen.syntax.stringifiers.spaced
import llvm.codegen.syntax.types.IRTypes

/**
 * https://llvm.org/docs/LangRef.html#br-instruction
 */
class IRInstructionBr(cond: IRTypes.I1Type, ifTrue: IRTypes.LabelType, ifFalse: IRTypes.LabelType) :
    IRSimpleInstruction({
        listOf(
            listOf("br".rawIR, IRTypes.I1, cond).spaced,
            listOf(IRTypes.Label, ifTrue).spaced,
            listOf(IRTypes.Label, ifFalse).spaced,
        )
    })

fun br(cond: IRTypes.I1Type, ifTrue: IRTypes.LabelType, ifFalse: IRTypes.LabelType) = IRInstructionRet()
