package llvm.codegen.instructions.terminator

import llvm.codegen.IRValueLike
import llvm.codegen.instructions.IRSimpleInstruction
import llvm.codegen.syntax.IRIdentifier
import llvm.codegen.syntax.IRPieceLike
import llvm.codegen.syntax.stringifiers.*
import llvm.codegen.syntax.types.IRTypes

/**
 * https://llvm.org/docs/LangRef.html#switch-instruction
 */
class IRInstructionSwitch(
    intType: IRTypes.Integer,
    value: IRTypes.IntegerType,
    defaultDest: IRIdentifier,
    destinations: List<Triple<IRTypes.Integer, IRTypes.IntegerType, IRIdentifier>>,
) :
    IRSimpleInstruction({
        listOf(
            listOf(
                listOf("switch".rawIR, intType, value).spaced,
                listOf(IRTypes.Label, defaultDest).spaced,
            ).commaSeparated,
            destinations.map { (t, v, d) ->
                listOf(listOf(t, v).spaced, listOf(IRTypes.Label, d).spaced).commaSeparated
            }.spaced.padded.bracketed,
        )
    })

fun switch(
    intType: IRTypes.Integer,
    value: IRTypes.IntegerType,
    defaultDest: IRIdentifier,
    destinations: List<Triple<IRTypes.Integer, IRTypes.IntegerType, IRIdentifier>>,
) = IRInstructionSwitch(intType, value, defaultDest, destinations)
