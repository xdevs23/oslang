package llvm.codegen.instructions.terminator

import llvm.codegen.IRValueLike
import llvm.codegen.instructions.IRSimpleInstruction
import llvm.codegen.syntax.IRPieceLike
import llvm.codegen.syntax.stringifiers.ir
import llvm.codegen.syntax.stringifiers.rawIR

/**
 * https://llvm.org/docs/LangRef.html#ret-instruction
 */
class IRInstructionRet(value: IRValueLike? = null) : IRSimpleInstruction({ listOfNotNull("ret".rawIR, value) })

fun ret(value: IRValueLike? = null) = IRInstructionRet(value)
