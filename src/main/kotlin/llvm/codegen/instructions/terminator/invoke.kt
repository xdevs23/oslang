package llvm.codegen.instructions.terminator

import llvm.CallingConvention
import llvm.CallingConventions
import llvm.codegen.IRValue
import llvm.codegen.instructions.IRInstruction
import llvm.codegen.paramattrs.ParameterAttribute
import llvm.codegen.paramattrs.ParameterAttributes
import llvm.codegen.syntax.IRBasicBlockAddress
import llvm.codegen.syntax.IRPieceLike
import llvm.codegen.syntax.stringifiers.*
import llvm.codegen.syntax.types.IRType
import llvm.codegen.syntax.types.IRTypes

/**
 * https://llvm.org/docs/LangRef.html#invoke-instruction
 */
class IRInstructionInvoke(
    private val callingConvention: CallingConvention = CallingConventions.C,
    private val returnAttributes: List<ParameterAttribute> = listOf(),
    // TODO: addrspace(...) ?
    private val addrSpace: IRPieceLike? = null,
    private val returnValueType: IRType? = null,
    private val functionType: IRTypes.Function? = null,
    private val functionPtrValue: IRType,
    private val functionArgs: List<Pair<IRType, IRValue>>,
    private val functionAttributes: List<ParameterAttribute> = listOf(),
    // TODO: ?
    private val operandBundles: IRPieceLike? = null,
    private val normalLabel: IRTypes.LabelType,
    private val exceptionLabel: IRTypes.LabelType,
) : IRInstruction() {
    init {
        returnAttributes.forEach {
            require(it in listOf(ParameterAttributes.ZeroExt, ParameterAttributes.SignExt, ParameterAttributes.InReg))
        }
        require((returnValueType != null) xor (functionType != null))
    }

    override fun piece() = listOfNotNull(
        listOfNotNull("invoke".rawIR, callingConvention, addrSpace, returnValueType, functionType).spaced,
        listOf(functionPtrValue, functionArgs.map { (t, v) -> listOf(t, v).spaced }.commaSeparated.enclosed).combined,
        functionAttributes.spaced, operandBundles,
        "to".rawIR, IRTypes.Label, normalLabel, "unwind".rawIR, IRTypes.Label, exceptionLabel
    ).spaced
}

fun invoke(
    address: IRBasicBlockAddress,
    destinations: List<IRTypes.LabelType>,
) = IRInstructionIndirectBr(address, destinations)
