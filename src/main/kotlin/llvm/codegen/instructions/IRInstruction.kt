package llvm.codegen.instructions

import llvm.codegen.syntax.IRDelegatedPiece
import llvm.codegen.syntax.IRPiece
import llvm.codegen.syntax.IRPieceLike
import llvm.codegen.syntax.stringifiers.spaced

// https://llvm.org/docs/LangRef.html#instruction-reference

interface IRInstructionLike
abstract class IRInstruction : IRDelegatedPiece(), IRInstructionLike
abstract class IRSimpleInstruction(private val gen: () -> List<IRPieceLike>) : IRInstruction() {
    override fun piece() = gen().spaced
}
