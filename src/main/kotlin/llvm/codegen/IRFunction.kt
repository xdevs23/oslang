package llvm.codegen

import llvm.codegen.syntax.*
import llvm.codegen.syntax.stringifiers.*
import llvm.codegen.syntax.types.IRType
import llvm.codegen.syntax.types.IRTypes
import java.util.LinkedList

typealias IRFunctionParameters = List<Pair<IRType, IRValueIdentifier>>

class IRFunction(
    private val context: IRContext,
    private val returnType: IRType,
    private val identifier: IRValueIdentifier,
    private val parameters: IRFunctionParameters,
    body: IRFunction.() -> Unit,
) : IRMultiBlock() {
    private val basicBlocks = LinkedList<IRBasicBlock>()

    init {
        body()
    }

    fun addBasicBlock(basicBlock: IRBasicBlock) {
        basicBlocks += basicBlock
    }

    operator fun IRBasicBlock.unaryPlus() {
        addBasicBlock(this)
    }

    infix fun String.basicBlock(block: IRBasicBlock.() -> Unit): IRTypes.LabelType =
        IRTypes.NamedLabel(this).also { label ->
            +IRBasicBlock(context, label, block)
        }

    fun entry(block: IRBasicBlock.() -> Unit) = "entry".basicBlock(block)

    override fun blocks() =
        (listOf(listOf(IRFunctionHeader(returnType, identifier, parameters), "{".rawIR).spaced) + basicBlocks + "}".rawIR)
            .block.inList
}

/**
 * https://releases.llvm.org/2.2/docs/tutorial/JITTutorial1.html
 */
class IRFunctionHeader(
    private val returnType: IRType,
    private val identifier: IRValueIdentifier,
    private val parameters: IRFunctionParameters,
) : IRDelegatedPiece() {
    override fun piece() = listOf(
        returnType,
        listOf(identifier, parameters.map { (t, i) -> listOf(t, i).spaced }.commaSeparated.enclosed).combined
    ).spaced
}