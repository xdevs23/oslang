package llvm.codegen.syntax.types

import llvm.codegen.syntax.IRPieceLike
import llvm.codegen.syntax.stringifiers.angleBracketed
import llvm.codegen.syntax.stringifiers.commaSeparated
import llvm.codegen.syntax.stringifiers.curlyEnclosed
import llvm.codegen.syntax.stringifiers.padded

interface IRStructureLike : IRTypeLike

/**
 * https://llvm.org/docs/LangRef.html#structure-type
 */
class IRStructureType(
    private val types: List<IRType>, private val packed: Boolean = false,
) : IRType(), IRStructureLike {
    override fun piece(): IRPieceLike =
        types.commaSeparated.padded.curlyEnclosed.let { if (packed) it.angleBracketed else it }
}

/**
 * https://llvm.org/docs/LangRef.html#opaque-structure-types
 */
class IROpaqueStructureType : IRNamedType("opaque"), IRStructureLike
