package llvm.codegen.syntax.types

import llvm.codegen.IRValue
import llvm.codegen.IRValueLike
import llvm.codegen.syntax.IRDelegatedPiece
import llvm.codegen.syntax.IRPieceLike
import llvm.codegen.syntax.stringifiers.*

interface IRTypeLike : IRPieceLike
/** https://llvm.org/docs/LangRef.html#t-firstclass */
interface IRFirstClassType : IRTypeLike, IRValueLike

/**
 * https://llvm.org/docs/LangRef.html#type-system
 */
abstract class IRType : IRDelegatedPiece()

open class IRNamedType(private val name: String) : IRType() {
    override fun piece() = SimpleIRPiece { name }
}

object IRTypes {
    /**
     * https://llvm.org/docs/LangRef.html#void-type
     */
    interface VoidType
    object Void : IRNamedType("void")

    /**
     * https://llvm.org/docs/LangRef.html#function-type
     */
    interface FunctionType
    class Function(private val returnType: IRType, private val parameters: List<IRType>) : IRType() {
        override fun piece() = listOf(returnType, parameters.commaSeparated.enclosed).spaced
    }

    // https://llvm.org/docs/LangRef.html#first-class-types
    // https://llvm.org/docs/LangRef.html#single-value-types

    /**
     * https://llvm.org/docs/LangRef.html#integer-type
     */
    interface IntegerType : IRFirstClassType
    open class Integer(bitWidth: Int) : IRNamedType("i$bitWidth")
    interface I1Type : IntegerType
    object I1 : Integer(1)
    interface I8Type : IntegerType
    object I8 : Integer(8)
    interface I16Type : IntegerType
    object I16 : Integer(16)
    interface I32Type : IntegerType
    object I32 : Integer(32)
    interface I64Type : IntegerType
    object I64 : Integer(64)

    /**
     * https://llvm.org/docs/LangRef.html#floating-point-types
     */
    interface FloatingPointType : IRFirstClassType
    abstract class FloatingPoint(name: String) : IRNamedType(name)
    object HalfFP : FloatingPoint("half")
    object BFloat : FloatingPoint("bfloat")
    object Float : FloatingPoint("float")
    object Double : FloatingPoint("double")
    object FP128 : FloatingPoint("fp128")
    object X86FP80 : FloatingPoint("x86_fp80")
    object PPCFP128 : FloatingPoint("ppc_fp128")

    /**
     * https://llvm.org/docs/LangRef.html#x86-amx-type
     */
    interface X86AMXType : IRFirstClassType
    object X86AMX : IRNamedType("x86_amx")

    /**
     * https://llvm.org/docs/LangRef.html#x86-mmx-type
     */
    interface X86MMXType : IRFirstClassType
    object X86MMX : IRNamedType("x86_mmx")

    /**
     * https://llvm.org/docs/LangRef.html#pointer-type
     */
    interface PointerLike
    interface PointerType : IRFirstClassType, PointerLike
    object Pointer : IRNamedType("ptr")

    /**
     * https://llvm.org/docs/LangRef.html#pointer-type
     */
    interface VectorType : IRFirstClassType
    class Vector(val scalable: Boolean = false, numberOfElements: Int, type: IRType) : IRType() {
        private val pieces = listOf(numberOfElements.ir, "x".rawIR, type)
        private val scalablePieces = listOf("vscale".rawIR, "x".rawIR)

        override fun piece() = (if (scalable) (scalablePieces + pieces) else pieces).spaced.angleBracketed
    }

    /**
     * https://llvm.org/docs/LangRef.html#label-type
     */
    interface LabelType : IRFirstClassType
    object Label : IRNamedType("label")
    class NamedLabel(val name: String) : IRValue(), LabelType {
        override val type = Label
        override fun value() = name.rawIR.withColonSuffix
    }

    /**
     * https://llvm.org/docs/LangRef.html#token-type
     */
    interface TokenType : IRFirstClassType
    object Token : IRNamedType("token")

    /**
     * https://llvm.org/docs/LangRef.html#metadata-type
     */
    interface MetadataType : IRFirstClassType
    object Metadata : IRNamedType("metadata")

    abstract class ComplexConstant : IRType()
    abstract class NamedComplexConstant(private val name: String) : ComplexConstant() {
        override fun piece() = name.rawIR
    }

}
