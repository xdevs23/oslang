package llvm.codegen.syntax

import llvm.codegen.syntax.stringifiers.*
import llvm.codegen.syntax.types.IRTypes

/**
 * https://llvm.org/docs/LangRef.html#addresses-of-basic-blocks
 */
class IRBasicBlockAddress(
    private val funcIdentifier: IRIdentifier, private val blockIdentifier: IRIdentifier,
) : IRDelegatedPiece(), IRTypes.PointerLike {
    override fun piece() =
        listOf("blockaddress".rawIR, listOf(funcIdentifier, blockIdentifier).commaSeparated.enclosed).combined
}
