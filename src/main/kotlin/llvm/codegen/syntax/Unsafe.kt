package llvm.codegen.syntax

import llvm.codegen.syntax.stringifiers.SimpleIRPiece

/**
 * https://llvm.org/docs/LangRef.html#undefined-values
 */
class IRUndef : SimpleIRPiece({ "undef" })

/**
 * https://llvm.org/docs/LangRef.html#poison-values
 */
class IRPoison : SimpleIRPiece({ "poison" })
