package llvm.codegen.syntax

import llvm.codegen.syntax.stringifiers.bracketed
import llvm.codegen.syntax.stringifiers.commaSeparated
import llvm.codegen.syntax.stringifiers.ir
import llvm.codegen.syntax.stringifiers.rawIR
import llvm.codegen.syntax.types.IRType

class IRArrayType(val size: Int, val type: IRType) : IRType() {
    override fun piece() = listOf(size.ir, "x".rawIR, type).commaSeparated.bracketed
}