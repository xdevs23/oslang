package llvm.codegen.syntax

import llvm.codegen.syntax.stringifiers.ir
import llvm.codegen.syntax.stringifiers.rawIr
import llvm.codegen.syntax.stringifiers.spaced

/**
 * https://llvm.org/docs/LangRef.html#no-cfi
 */
class IRNoCFI(private val funcIdentifier: IRIdentifier) : IRDelegatedPiece() {
    override fun piece() = listOf("no_cfi".rawIr, funcIdentifier).spaced
}
