package llvm.codegen.syntax.stringifiers

import llvm.codegen.IRValue
import llvm.codegen.constants.FalseBooleanConstant
import llvm.codegen.constants.TrueBooleanConstant
import llvm.codegen.syntax.IRPiece
import llvm.codegen.syntax.IRPieceLike
import llvm.codegen.syntax.types.IRType
import llvm.codegen.syntax.types.IRTypes

interface IRPieceFromString
interface IRRawPiece
interface IRPieceFromList

abstract class GeneratedIRPiece : IRPiece() {
    abstract fun generator(): String
    override fun text() = generator()
}

open class SimpleIRPiece(val gen: () -> String) : GeneratedIRPiece() {
    override fun generator() = gen()
}

open class SimpleValueIRPiece(private val gen: () -> String, override val type: IRType) : IRValue() {
    override fun value() = SimpleIRPiece(gen)
}

open class ValueIRPiece(private val gen: () -> IRPieceLike, override val type: IRType) : IRValue() {
    override fun value() = gen()
}

class EnclosedIRPiece(piece: IRPieceLike) : SimpleIRPiece({ "($piece)" }), IRPieceFromString
class CurlyEnclosedIRPiece(piece: IRPieceLike) : SimpleIRPiece({ "{$piece}" }), IRPieceFromString
class BracketEnclosedIRPiece(piece: IRPieceLike) : SimpleIRPiece({ "[$piece]" }), IRPieceFromString
class AngledBracketedIRPiece(piece: IRPieceLike) : SimpleIRPiece({ "< $piece >"}), IRPieceFromString
class CommaSeparatedIRPiece(pieces: List<IRPieceLike>) :
    SimpleIRPiece({ pieces.joinToString(", ") }), IRPieceFromList
class SpaceSeparatedIRPiece(pieces: List<IRPieceLike>) :
    SimpleIRPiece({ pieces.joinToString(" ") }), IRPieceFromList
class CombinedIRPiece(pieces: List<IRPieceLike>) :
    SimpleIRPiece({ pieces.joinToString("") }), IRPieceFromList
class PaddedIRPiece(piece: IRPieceLike, amount: Int = 1, start: Boolean = true, end: Boolean = true) : SimpleIRPiece({
    "${" ".repeat(if (start) amount else 0)}$piece${" ".repeat(if (end) amount else 0)}"
}), IRPieceFromString
class IRPieceWithColonSuffix(piece: IRPieceLike) : SimpleIRPiece({ "$piece:" })
class NumberIRPiece(num: Number) : SimpleIRPiece({ "$num" }), IRPieceFromString
class BooleanIRPiece(bool: Boolean) : SimpleIRPiece({ "$bool" }), IRPieceFromString
class StringIRPiece(str: String) : SimpleIRPiece({ "\"$str\"" }), IRPieceFromString
class RawIRPiece(text: String) : SimpleIRPiece({ text }), IRPieceFromString, IRRawPiece

inline val IRPieceLike.enclosed get() = EnclosedIRPiece(this)
inline val IRPieceLike.curlyEnclosed get() = CurlyEnclosedIRPiece(this)
inline val IRPieceLike.bracketed get() = BracketEnclosedIRPiece(this)
inline val IRPieceLike.padded get() = PaddedIRPiece(this)
inline val IRPieceLike.angleBracketed get() = AngledBracketedIRPiece(this)
inline val IRPieceLike.withColonSuffix get() = IRPieceWithColonSuffix(this)
inline val List<IRPieceLike>.commaSeparated get() = CommaSeparatedIRPiece(this)
inline val List<IRPieceLike>.spaced get() = SpaceSeparatedIRPiece(this)
inline val List<IRPieceLike>.combined get() = CombinedIRPiece(this)
inline val Number.rawIr get() = NumberIRPiece(this)
inline val Boolean.rawIr get() = BooleanIRPiece(this)
inline val String.rawIr get() = StringIRPiece(this)
inline val String.rawIR get() = RawIRPiece(this)

inline val Boolean.ir get() =
    ValueIRPiece({ if (this) TrueBooleanConstant else FalseBooleanConstant }, IRTypes.I1)
inline val Byte.ir get() = ValueIRPiece({ rawIr }, IRTypes.I8)
inline val UByte.ir get() = ValueIRPiece({ SimpleIRPiece { "$this" } }, IRTypes.I8)
inline val Short.ir get() = ValueIRPiece({ rawIr }, IRTypes.I16)
inline val UShort.ir get() = ValueIRPiece({ SimpleIRPiece { "$this" } }, IRTypes.I16)
inline val Int.ir get() = ValueIRPiece({ rawIr }, IRTypes.I32)
inline val UInt.ir get() = ValueIRPiece({ SimpleIRPiece { "$this" } }, IRTypes.I32)
inline val Long.ir get() = ValueIRPiece({ rawIr }, IRTypes.I64)
inline val ULong.ir get() = ValueIRPiece({ SimpleIRPiece { "$this" } }, IRTypes.I64)
