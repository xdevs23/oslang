package llvm.codegen.syntax

import llvm.codegen.defaultIndentation

interface IRPieceLike {
    fun text(): String
    override fun toString(): String
}

/**
 * IRPiece is the class all code pieces inherit from.
 *
 * Define necessary interfaces here so that it can be propagated
 * to all other code pieces.
 *
 * For now, we will focus on human-readable LLVM IR assembly but
 * this could be extended to also accommodate bitcode generation
 */
abstract class IRPiece : IRPieceLike {
    override fun toString() = text()
}

abstract class IRDelegatedPiece : IRPiece() {
    abstract fun piece(): IRPieceLike

    override fun text() = piece().text()
}

interface IRBlockLike

abstract class IRBlock : IRPiece(), IRBlockLike {
    open val indentation: Int = defaultIndentation
    abstract fun pieces(): List<IRPieceLike>
    override fun text() = pieces().joinToString("\n") { it.text() }.lineSequence().joinToString("\n") {
        it.prependIndent(" ".repeat(indentation))
    }
}

open class IRSimpleBlock(val pieceGen: () -> List<IRPieceLike>) : IRBlock() {
    override fun pieces() = pieceGen()
}

val List<IRPieceLike>.block get() = IRSimpleBlock { this }
val IRBlock.inList get() = listOf(this)

abstract class IRMultiBlock : IRPiece(), IRBlockLike {
    abstract fun blocks(): List<IRBlockLike>
    override fun text() = blocks().joinToString("\n")
}
