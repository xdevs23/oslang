package llvm.codegen.syntax

private val identifierNameRegex = Regex("[-a-zA-Z\$._][-a-zA-Z\$._0-9]*")

private const val globalIdentifierPrefix = '@'
private const val localIdentifierPrefix = '%'

/**
 * https://llvm.org/docs/LangRef.html#identifiers
 */
abstract class IRIdentifier : IRPiece()

/**
 * 1. https://llvm.org/docs/LangRef.html#identifiers
 */
open class IRValueIdentifier(
    val name: String, val local: Boolean, val suppressMangling: Boolean = false,
) : IRIdentifier() {

    private val filteredName get() =
        if (identifierNameRegex.matches(name)) name
        else "\"$name\""

    private val localityIdentifierPrefix get() =
        if (local) localIdentifierPrefix else globalIdentifierPrefix

    private val manglingIdentifierPrefix get() =
        if (!suppressMangling) "" else Char(0x01).toString()

    private val identifierPrefix get() = "$manglingIdentifierPrefix$localityIdentifierPrefix"

    override fun text() = "$identifierPrefix$filteredName"
}

/**
 * 2. https://llvm.org/docs/LangRef.html#identifiers
 */
class IRUnnamedValueIdentifier(number: UInt, local: Boolean) : IRValueIdentifier("$number", local)

val String.globalIdentifier get() = IRValueIdentifier(this, local = false)
val String.localIdentifier get() = IRValueIdentifier(this, local = true)
