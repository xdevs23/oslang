package llvm.codegen.syntax

/**
 * 2 1. https://llvm.org/docs/LangRef.html#identifiers
 */
class IRComment(private val text: String) : IRPiece() {
    private val filteredText get() = text.lineSequence().joinToString("; ", prefix = "; ")

    override fun text() = filteredText
}
