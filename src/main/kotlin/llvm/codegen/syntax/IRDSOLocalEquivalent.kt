package llvm.codegen.syntax

import llvm.codegen.syntax.stringifiers.ir
import llvm.codegen.syntax.stringifiers.rawIR
import llvm.codegen.syntax.stringifiers.spaced

/**
 * https://llvm.org/docs/LangRef.html#dso-local-equivalent
 */
class IRDSOLocalEquivalent(private val funcIdentifier: IRIdentifier) : IRDelegatedPiece() {
    override fun piece() = listOf("dso_local_equivalent".rawIR, funcIdentifier).spaced
}
