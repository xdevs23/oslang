package llvm.codegen

class IRModule(context: IRContext, code: IRModule.() -> Unit) : IRBlockWithBody<IRModule>(context, code) {
    init {
        code()
    }
}
