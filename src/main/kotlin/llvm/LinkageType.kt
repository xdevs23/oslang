package llvm

import llvm.codegen.syntax.IRPiece

/**
 * https://llvm.org/docs/LangRef.html#linkage-types
 */
abstract class LinkageType : IRPiece()

/**
 * https://llvm.org/docs/LangRef.html#linkage-types
 */
object LinkageTypes {

    object Private : LinkageType() {
        override fun text() = "private"
    }

    object Internal : LinkageType() {
        override fun text() = "internal"
    }

    object AvailableExternally : LinkageType() {
        override fun text() = "available_externally"
    }

    object LinkOnce : LinkageType() {
        override fun text() = "linkonce"
    }

    object Weak : LinkageType() {
        override fun text() = "weak"
    }

    object Common : LinkageType() {
        override fun text() = "common"
    }

    object Appending : LinkageType() {
        override fun text() = "appending"
    }

    object ExternWeak : LinkageType() {
        override fun text() = "extern_weak"
    }

    // ODR = "one definition rule"
    object LinkOnceODR : LinkageType() {
        override fun text() = "linkonce_odr"
    }

    // ODR = "one definition rule"
    object WeakODR : LinkageType() {
        override fun text() = "weak_odr"
    }

    object External : LinkageType() {
        override fun text() = "external"
    }

}
