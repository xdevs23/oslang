package llvm

import llvm.codegen.syntax.IRPiece

/**
 * https://llvm.org/docs/LangRef.html#runtime-preemption-specifiers
 */
abstract class RuntimePreemptionSpecifier : IRPiece()

/**
 * https://llvm.org/docs/LangRef.html#runtime-preemption-specifiers
 */
object RuntimePreemptionSpecifiers {

    object DSOPreemptable : RuntimePreemptionSpecifier() {
        override fun text() = "dso_preemptable"
    }

    object DSOLocal : RuntimePreemptionSpecifier() {
        override fun text() = "dso_local"
    }

}
