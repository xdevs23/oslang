package llvm

import llvm.codegen.syntax.IRPiece

/**
 * https://llvm.org/docs/LangRef.html#calling-conventions
 */
abstract class CallingConvention : IRPiece()

/**
 * https://llvm.org/docs/LangRef.html#calling-conventions
 */
object CallingConventions {
    object C : CallingConvention() {
        override fun text() = "ccc"
    }

    object Fast : CallingConvention() {
        override fun text() = "fastcc"
    }

    object Cold : CallingConvention() {
        override fun text() = "coldcc"
    }

    object GHC : NumberedCallingConvention(10)
    object HiPE : NumberedCallingConvention(11)

    object WebKitJS : CallingConvention() {
        override fun text() = "webkit_jscc"
    }

    object DynamicForCodePatching : CallingConvention() {
        override fun text() = "anyregcc"
    }

    object PreserveMost : CallingConvention() {
        override fun text() = "preserve_mostcc"
    }

    object PreserveAll : CallingConvention() {
        override fun text() = "preserve_allcc"
    }

    object CXXFastTLS : CallingConvention() {
        override fun text() = "cxx_fast_tlscc"
    }

    object Tail : CallingConvention() {
        override fun text() = "tailcc"
    }

    object Swift : CallingConvention() {
        override fun text() = "swiftcc"
    }

    object SwiftTail : CallingConvention() {
        override fun text() = "swifttailcc"
    }

    object WindowsControlFlowGuard : CallingConvention() {
        override fun text() = "cfguard_checkcc"
    }

    open class NumberedCallingConvention(private val number: Int) : CallingConvention() {
        init {
            require(number >= 0)
        }
        override fun text() = "cc $number"
    }

}
