package llvm

import llvm.codegen.syntax.IRPiece

/**
 * https://llvm.org/docs/LangRef.html#visibility-styles
 */
abstract class VisibilityStyle : IRPiece()

/**
 * https://llvm.org/docs/LangRef.html#visibility-styles
 */
object VisibilityStyles {

    object Default : VisibilityStyle() {
        override fun text() = "default"
    }

    object Hidden : VisibilityStyle() {
        override fun text() = "hidden"
    }

    object Protected : VisibilityStyle() {
        override fun text() = "protected"
    }

}
