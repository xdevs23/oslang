import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.7.20"
}

group = "dev.superboring"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    testImplementation(kotlin("test"))
}

tasks.test {
    useJUnitPlatform()
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}

fun jar(name: String, mainClass: String) {
    task(name, type = Jar::class) {
        archiveBaseName.set(project.name)
        manifest {
            attributes["Main-Class"] = mainClass
        }
        from(configurations.runtimeClasspath.get().map { if (it.isDirectory) it else zipTree(it) })
        with(tasks["jar"] as CopySpec)
        duplicatesStrategy = DuplicatesStrategy.WARN
    }
}

jar("exampleHelloWorldJar", "examples.LLVMHelloWorldKt")
